# Google Cloud Platform を使う準備

## Google Cloud Platform にアカウント登録する
- 自身のメールアドレスでサインアップしてください
- サインアップ後に、GCPに

## GKE を使うために、無料トライアルに登録する

- GKE(Kubernetes Engine) を使うためには無料トライアルに登録する必要がある
  - 無料トライアルに登録する際にクレジットカード情報を登録する必要がある
  - 初めて利用する場合は無料枠が有効になるので、費用はかからない
  - 無料トライアル期間を既に使ってしまった人は、多少の費用（数百円程度）がかかる
- GUIから操作する
- 無料トライアルを開始から始める
<img src="image/requirement-for-gke.png" width="80%" />

- 必要事項を入力
<img src="image/setup-trial-1.png" width="50%" />

- クレジットカード登録が必要
<img src="image/setup-trial-2.png" width="50%" />



## GCPに新しいプロジェクトを作成

- GUIから作成する
  - `devops-test-{something}` という名称で作成
- 完了後にそのプロジェクトを削除すれば、プロジェクトと関連するすべてのリソースを削除できるので、ハンズオンで使うプロジェクトを新規に作成する
- 終了後に登録したプロジェクトを削除する予定

<img src="image/create-project.png" width="50%" />

**おつかれさまでした。次のセクションに進みます。**

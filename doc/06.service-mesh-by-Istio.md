# Istioのサービスメッシュを利用してみる

## Istioをインストール

- Istioをインストール

```
$ cd && \
  curl -L https://git.io/getLatestIstio | ISTIO_VERSION=1.2.5 sh -
```

```
$ cd istio-1.2.5 && \
  export PATH=$PWD/bin:$PATH && \
  kubectl apply -f install/kubernetes/istio-demo.yaml
```

- インストールの確認

```
$ kubectl get svc -n istio-system && \
  kubectl get pods -n istio-system && \
  istioctl version
```

- Tips: ダウンロードした `istio-1.2.5` のディレクトリにサンプルがあるので、Istio の設定に困ったときに参考にできます


## 取得サンプルコードで、Istioを使ったデプロイを行ってみる


- defaultネームスペースでIstio（Envoy Proxy）のsidecar auto-injectionを有効にする
  - `istioctl kube-inject -f k8s/service.yaml` のように Istio の Proxy を注入したファイルを、K8sにデプロイするが、自動注入してくれる

```
$ kubectl label namespace default istio-injection=enabled
```

- Cloneしたテストコードをデプロイ！

```
$ cd ~/devops-test && \
  export PROJECT=$(gcloud info --format='value(config.project)') && \
  sed -i s/devops-test/devops-test:v0.0.1/g k8s/deployment* && \
  sed -i s/project_name/$PROJECT/g k8s/deployment* && \
  sed -i s/LoadBalancer/ClusterIP/g k8s/service.yaml && \
  kubectl apply -f k8s/deployment.yaml && \
  kubectl apply -f k8s/deployment-canary.yaml && \
  kubectl apply -f k8s/service.yaml && \
  kubectl apply -f istio/gateway.yaml && \
  kubectl apply -f istio/destinationrule.yaml && \
  kubectl apply -f istio/virtualservice.yaml
```

- デプロイしたものの状態を確認

```
$ kubectl get pod
$ kubectl get service
$ kubectl get gateway
$ kubectl get destinationrules
$ kubectl get virtualservices
```

- ブラウザでアクセスしてみる

```
$ kubectl get svc istio-ingressgateway -n istio-system
```

- `kubectl get svc istio-ingressgateway -n istio-system` コマンドの結果の EXTERNAL-IP にブラウザでアクセス
- ブラウザでアクセスすると1回毎に、 `production` と `canary` にアクセスが切り替わる
  - 4章では「ランダム」だったが、この時点では「ラウンドロビン」で振り分けを行っている


## Istioを使ってルーティングのルールを変更してみる

- ロードバランサーのタイプを、ラウンドロビンからランダムに変更
  - ブラウザでアクセスすると、ランダムでアクセスするページが切り替わります（４章と同じ状態だけど振り分けはIstioが実施している）

```
$ kubectl apply -f istio/destinationrule-random.yaml
```

- 9割はプロダクションにトラフィックを通し、1割をカナリアへ転送

```
$ kubectl apply -f istio/virtualservice-90-10.yaml
```

- 全てプロダクションへ

```
$ kubectl apply -f istio/virtualservice-only-prod.yaml
```

- カナリア環境を使うユーザはカナリアへ
  - いわゆるアーリーアダプタに対し「新しい機能を有効にする」ボタンを提供する機能

```
$ kubectl apply -f istio/virtualservice-cookie.yaml
```




## (オプション) Istioと同時に導入されるモニタリングツールを確認する

- 自動的に Prometheus と Grafana がインストールされているので使ってみる
- まず、デプロロイされているか確認

```
$ kubectl -n istio-system get svc prometheus && \
  kubectl -n istio-system get svc grafana
```

- Grafana UI から Istio のダッシュボードを表示するには、まずは以下のコマンドでポート（3000）の転送設定

```
$ kubectl -n istio-system port-forward $(kubectl -n istio-system get pod -l app=grafana -o  \
     jsonpath='{.items[0].metadata.name}') 3000:3000 >> /dev/null &
```

- ウェブでプレビュー > ポートを変更

<img src="image/change-preview-port.png" width="70%" />

- ポートを3000でプレビュー開始

<img src="image/start-preview-grafana.png" width="70%" />

- 動いていることだけ確認する（Prometheus と Grafana についての説明は割愛）
- トップ画面
![](image/grafana-top.png)

- Workloadのダッシュボード
![](image/grafana-sample.png)


## クリーンアップ

- 次のセッションは、このままの環境を使って進めるので、この章ではクリーンアップを行いません


**おつかれさまでした。次のセクションに進みます。**

